import { computed, ReadonlySignal } from "@preact/signals";
import { UndoManager, Undoable, Preservable } from "@kvndy/undo-manager";

import { ToWords } from "to-words";
const toWords = new ToWords();

type Id = number;
type Node = {
	text: string,
	childIds: Array<Id>
};
type Tree = Record<Id, Node>
type Model = {
	tree: Undoable,
	nextId: Id // lol
};
type Presentation = {
	allCollapsedIds: Preservable,
	selectedId: Preservable,
	listScroll: Preservable
};
type Getters = {
	getText: Function,
	getChildIds: Function
};
type Setters = {
	setText: Function,
	setScroll: Function,
	disclosureToggle: Function,
	addItem: Function,
	deleteSelectedItem: Function,
	selectListItem: Function
};
type Derived = {
	flattenedIdsLinear: ReadonlySignal,
	flattenedDepthLookup: ReadonlySignal,
	flattenedVisibleIds: ReadonlySignal,
	visibleCollapsedIds: ReadonlySignal,
	parentIds: ReadonlySignal
};
type State = {
	model: Model,
	presentation: Presentation,
	getters: Getters,
	setters: Setters,
	derived: Derived
};

const getNode = function(id: Id, tree: Tree) {
	const key: number = id;
	return tree[key];
};
const setNode = function(node: Node, id: Id, tree: Tree) {
	const key: number = id;
	tree[key] = node;
};
const deleteNode = function(id: Id, tree: Tree) {
	const key: number = id;
	delete tree[key];
};

const textFromId = function(id: Id) {
	const num: number = id;
	return toWords.convert(num).toLowerCase().replace("-", " ").replace(",", "");
};
const incrementId = function(id: Id) { // lol
	const num: number = id;
	return num + 1;
};

const randomTree = function(length: number) : Tree {
	const tree: Tree = Object.create(null);
	if (length < 0) length = 0;
	for (let i=0; i<length+1; i++) {
		const id: Id = i;
		const child: Node = {
			text: textFromId(id),
			childIds: []
		};
		setNode(child, id, tree);
		if (i > 0) {
			const parentId: Id = Math.floor(Math.random() * i);
			tree[parentId].childIds.push(id);
		}
	}
	return tree;
};

const establishGetters = function(model: Model) : Getters {
	const getText = function(id: Id) {
		const tree: Tree = model.tree.value;
		const node: Node = getNode(id, tree);
		if (!node) return "";
		return node.text;
	}
	const getChildIds = function(id: Id) {
		const tree: Tree = model.tree.value;
		const node: Node = getNode(id, tree);
		if (!node) return [];
		return node.childIds;
	}
	return {
		getText,
		getChildIds,
	}
}

const establishSetters = function(model: Model, presentation: Presentation, manager: UndoManager, derived: Derived) : Setters {
	const { group } = manager;
	const shallow_copy = function(value: Object) {
		return Object.assign(Object.create(null), value);
	}

	const setText = function(nodeId: Id, text: string) {
		const dict: Tree = model.tree.value;
		const node: Node = getNode(nodeId, dict);
		if (node.text !== text) {
			const result: Tree = shallow_copy(dict);
			const item: Node = shallow_copy(node);
			item.text = text;
			setNode(item, nodeId, result);
			group( () => {
				model.tree.value = result;
			}, "set text of item with id " + nodeId, true);
		}
	};

	const setScroll = function(scrollTop: number) {
		presentation.listScroll.value = scrollTop;
	}

	const disclosureToggle = function(nodeId: Id) {
		const collapsed = presentation.allCollapsedIds.value.slice(0);
		const index = collapsed.indexOf(nodeId);
		if (index < 0) {
			collapsed.push(nodeId);
			const flattenedIds = derived.flattenedIdsLinear.value;
			var sortFunction = function(a: number, b: number) {
				var A = flattenedIds.indexOf(a);
				var B = flattenedIds.indexOf(b);
				return A - B;
			};
			collapsed.sort(sortFunction);
		} else {
			collapsed.splice(index, 1);
		}
		presentation.allCollapsedIds.value = collapsed;
	}

	const addItem = function() {
		const parentId = presentation.selectedId.value || 0;
		addChildToParentWithId(parentId);
	}

	const addChildToParentWithId = function(parentId: number) {
		const result = shallow_copy(model.tree.value);
		const childId = model.nextId;
		model.nextId = incrementId(childId);
		const parentNode: Node = getNode(parentId, result);
		const string = textFromId(childId);
		const parentNodeNext: Node = shallow_copy(parentNode);
		parentNodeNext.childIds = [...parentNode.childIds, childId];
		setNode(parentNodeNext, parentId, result);
		const childNode: Node = {
			text: string,
			childIds:[],
		};
		setNode(childNode, childId, result);
		
		const collapsed = presentation.allCollapsedIds.value.slice(0);
		let superId = parentId;
		while (superId) { // expand if inserted item is hidden by disclosure. Expects root node to be 0
			const index = collapsed.indexOf(superId);
			if (index !== -1) {
				collapsed.splice(index,1);
			}
			superId = derived.parentIds.value[superId];
		}

		group( () => {
			model.tree.value = result;
			presentation.selectedId.value = childId;
			presentation.allCollapsedIds.value = collapsed;
		}, "add child to parent with id " + parentId);	
	}

	const deleteSelectedItem = function(andReparentChildren: Boolean) {
		const id = presentation.selectedId.value;
		if (id > 0) { // can't delete top-level node
			let description = "delete item with id " + id;
			if (andReparentChildren) description += " and reparent children";
			else description += " and all descendants";
			group( () => {
				deleteItemWithId(id, andReparentChildren);
				presentation.selectedId.value = 0;
			}, description);
		}
	}

	const deleteItemWithId = function(id: Id, andReparentChildren: Boolean) {
		const tree = shallow_copy(model.tree.value);
		const allCollapsedIds = [...presentation.allCollapsedIds.value];
		const parentId = derived.parentIds.value[id];
		if (andReparentChildren) {
			deleteItemAndReparentChildren(id, parentId, tree, allCollapsedIds);
		} else { // Delete orphaned nodes
			deleteItemAndDescendants(id, parentId, tree, allCollapsedIds);
		}
		model.tree.value = tree; // lol
		presentation.allCollapsedIds.value = allCollapsedIds;
	}

	const deleteItemAndReparentChildren = function(id: Id, topParentId: Id, result: Tree, allCollapsedIds: Array<Id>) { // not recursive // mutates
		const node = getNode(id, result);
		const topParent: Node = getNode(topParentId, result);
		const topParentNode: Node = shallow_copy(topParent);
		const parentChildIds = [ ...topParentNode.childIds];
		const subIndex = parentChildIds.indexOf(id);
		parentChildIds.splice(subIndex, 1, ...node.childIds);
		topParentNode.childIds = parentChildIds;
		setNode(topParentNode, topParentId, result);
		deleteNode(id, result);
		const collapsedIndex = allCollapsedIds.indexOf(id);
		if (collapsedIndex > -1) allCollapsedIds.splice(collapsedIndex,1);
	}

	const deleteItemAndDescendants = function(id: Id, topParentId: Id, result: Tree, allCollapsedIds: Array<Id>) { // recursive // mutates
		const node = getNode(id, result);
		const childIds: Array<number> = node.childIds;
		childIds.forEach( childId => {
			deleteItemAndDescendants(childId, topParentId, result, allCollapsedIds);
		});
		const actualParentId = derived.parentIds.value[id];
		if (actualParentId === topParentId) {
			const topParent: Node = getNode(topParentId, result);
			const topParentNode: Node = shallow_copy(topParent);
			const parentChildIds = [ ...topParentNode.childIds];
			const subIndex = parentChildIds.indexOf(id);
			parentChildIds.splice(subIndex,1);
			topParentNode.childIds = parentChildIds;
			setNode(topParentNode, topParentId, result);
		}
		deleteNode(id, result);
		const collapsedIndex = allCollapsedIds.indexOf(id);
		if (collapsedIndex > -1) allCollapsedIds.splice(collapsedIndex,1);
	}

	const selectListItem = function(id: Id | undefined | null) {
		if (typeof id === "undefined" || id === null) presentation.selectedId.value = 0; // lol
		presentation.selectedId.value = id;
	}

	return {
		setText,
		setScroll,
		disclosureToggle,
		addItem,
		deleteSelectedItem,
		selectListItem
	};
};

const establishDerived = function(model: Model, presentation: Presentation) : Derived {
	const flattenedIdsLinear = computed(() => {
		const dict = model.tree.value;
		const populateFlattenedIdsLinear = function(nodeId: Id, destination: Array<number>) { // Does not include the root node
			const node = getNode(nodeId, dict);
			const childIds = node.childIds || [];
			const childrenLength = childIds.length;
			for (let childIndex=0; childIndex<childrenLength; childIndex++) {
				const childId = childIds[childIndex];
				const childNode = dict[childId];
				if (childNode) {
					destination.push(childId);
					populateFlattenedIdsLinear(childId, destination);
				}
			}
		}
		const result: Array<number> = [];
		populateFlattenedIdsLinear(0, result);
		return result;
	});

	const flattenedDepthLookup = computed(() => {
		const dict = model.tree.value;
		const populateFlattenedDepthLookup = function(nodeId: Id, depth: number, destination: Array<number>) { // Does not include the root node
			if (nodeId !== 0) destination.push(depth);
			const node = getNode(nodeId, dict);
			const childIds = node.childIds || [];
			const childrenLength = childIds.length;
			for (let childIndex=0; childIndex<childrenLength; childIndex++) {
				const childId = childIds[childIndex];
				const childNode = getNode(childId, dict);
				if (childNode) {
					populateFlattenedDepthLookup(childId, depth+1, destination);
				}
			}
		}
		const result: Array<number> = [];
		populateFlattenedDepthLookup(0, 0, result);
		return result;
	});

	const flattenedVisibleIds = computed(() => {
		const dict = model.tree.value;
		const flattenedIds = flattenedIdsLinear.value;
		const collapsed = presentation.allCollapsedIds.value;
		const result: Array<number> = [];
		populateVisible(result,dict,0,flattenedIds,collapsed,0,false);
		return result;
	});

	const populateVisible = function(
		destination: Array<number>,
		dict: Tree,
		nodeId: Id,
		flattenedIdsLinear: Array<number>,
		allCollapsedIds: Array<number>,
		collapsedIndex: number,
		ancestorCollapsed: Boolean
	) {
		const node = getNode(nodeId, dict);
		const childIds = node.childIds || [];
		const childrenLength = childIds.length;
		const collapsedLength = allCollapsedIds.length;
		for (var childIndex = 0; childIndex < childrenLength; childIndex++) {
			const childId = childIds[childIndex];
			const childNode = getNode(childId, dict);
			if (childNode) {
				var childCollapsed = false;
				while (collapsedIndex < collapsedLength) {
					var collapsedId = allCollapsedIds[collapsedIndex];
					var sortFunction = function(a: number, b: number) {
						var A = flattenedIdsLinear.indexOf(a);
						var B = flattenedIdsLinear.indexOf(b);
						return A - B;
					};
					var order = sortFunction(childId, collapsedId);
					if (order === 0) {
						childCollapsed = true;
						break;
					} else if (order < 0) break;
					collapsedIndex++;
				}
				if (!ancestorCollapsed) destination.push(childId);
				populateVisible(
					destination,
					dict,
					childId,
					flattenedIdsLinear,
					allCollapsedIds,
					collapsedIndex,
					ancestorCollapsed || childCollapsed
				);
			}
		}
	}

	const visibleCollapsedIds = computed(() => {
		const flattenedIds = flattenedIdsLinear.value;
		const visibleIds = flattenedVisibleIds.value;
		const collapsedIds = presentation.allCollapsedIds.value;
		const flattenedLength = flattenedIds.length;
		const visibleLength = visibleIds.length;
		const collapsedLength = collapsedIds.length;
		let visibleIndex = 0;
		let collapsedIndex = 0;
		const result: Array<number> = [];
		for (let flattenedIndex=0; flattenedIndex<flattenedLength; flattenedIndex++) {
			if (collapsedIndex >= collapsedLength || visibleIndex >= visibleLength) break;
			const flattenedId = flattenedIds[flattenedIndex];
			const visibleId = visibleIds[visibleIndex]
			const collapsedId = collapsedIds[collapsedIndex];
			if (flattenedId === collapsedId && flattenedId === visibleId) {
				result.push(collapsedId);
			}
			if (flattenedId === visibleId) visibleIndex++;
			if (collapsedId === flattenedId) collapsedIndex++;
		}
		return result;
	});

	const parentIds = computed(() => {
		const dict = model.tree.value;
		const ids = Object.keys(dict);
		const idsLength = ids.length;
		const result: Record<number, number> = Object.create(null);
		for (let i=0; i<idsLength; i++) {
			const string = ids[i];
			const number = Number(string);
			const item = dict[number];
			const childIds = item.childIds;
			const childLength = childIds.length;
			for (let j=0; j<childLength; j++) {
				const childId = childIds[j];
				result[childId] = number;
			}
		}
		return result;
	});
	return {
		flattenedIdsLinear,
		flattenedDepthLookup,
		flattenedVisibleIds,
		visibleCollapsedIds,
		parentIds
	}
};

const initState = function(manager: UndoManager, length: number | null | undefined) {
	if (!length || length < 0) length = 0;
	const { undoable, preservable } = manager;
	const model: Model = {
		tree: undoable( randomTree(length) ),
		nextId: length + 1, // lol
	}
	const presentation: Presentation = {
		allCollapsedIds: preservable( [] ),
		selectedId: preservable(0),
		listScroll: preservable(0),
	}
	const derived = establishDerived(model, presentation);
	const getters = establishGetters(model);
	const setters = establishSetters(model, presentation, manager, derived);
	return {
		model,
		presentation,
		getters,
		setters,
		derived
	}
}

export {
	initState,
	State,
	Model,
	Presentation,
	Getters,
	Setters,
	Derived,
	Tree,
	Node,
	Id
};
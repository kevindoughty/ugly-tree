import { h, FunctionComponent } from "preact";
import { Setters, Presentation } from "./state.js";
import { UndoManager } from "@kvndy/undo-manager";

type HeaderProps = {
	setters: Setters,
	presentation: Presentation,
	manager: UndoManager
};
const Header: FunctionComponent<HeaderProps> = ({ setters, presentation, manager }: HeaderProps) => {
	const { undo, redo, canUndo, canRedo, undoDescription, redoDescription } = manager;
	const handleAddChild = () => {
		setters.addItem();
	}
	const handleDeleteAndReparent = () => {
		setters.deleteSelectedItem(true);
	}
	const handleDeleteAndAllDescendants = () => {
		setters.deleteSelectedItem(false);
	}
	const handleDeselect = () => {
		presentation.selectedId.value = 0;
	}
	return (
		<header>
			<button onClick={ undo } title={ undoDescription.value } disabled={ !canUndo.value }>Undo</button>
			<button onClick={ redo } title={ redoDescription.value } disabled={ !canRedo.value }>Redo</button>
			<button onClick={ handleAddChild }>Add Child</button>
			<button onClick={ handleDeleteAndReparent } disabled={ !presentation.selectedId.value }>Delete Only</button>
			<button onClick={ handleDeleteAndAllDescendants } disabled={ !presentation.selectedId.value }>Delete All</button>
			<button onClick={ handleDeselect } disabled={!presentation.selectedId.value}>Deselect</button>
		</header>
	);
}
export default Header;
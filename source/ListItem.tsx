import { h, Component } from "preact";
import { useRef } from "preact/hooks";
import { Setters, Id } from "./state.js";

type ListItemProps = {
	id: Id,
	text: string,
	expandable: boolean,
	toggled: boolean,
	selected: boolean,
	depth: number,
	refReporter: Function,
	setters: Setters
};
type ListItemState = {};

class ListItem extends Component<ListItemProps, ListItemState> {
	item: HTMLElement | null = null;
	setRef = (dom: HTMLElement | null) => this.item = dom;

	componentDidMount() {
		const props = this.props;
		if (props && props.selected) {
			if (props.refReporter) {
				props.refReporter(this.item);
			}
		}
	}

	render() {
		const props = this.props;
		const ref = useRef(null);

		const {
			id,
			text,
			expandable,
			toggled,
			selected,
			depth
		} = props;

		const name = String(id); // lol

		const itemStyle = {
			"--depth": depth
		};

		function handleCheckChange(e: Event) {
			props.setters.disclosureToggle(id);
		}
		function handleCheckClick(e: Event) {
			e.stopPropagation(); // needed to prevent list item click
		}
		function handleClickListItem(e: Event) {
			props.setters.selectListItem(id);
		}
		function handleBlurText(e: Event) {
			const target = e.target;
			if (target) {
				const text: String = (target as HTMLInputElement).value;
				props.setters.setText(id, text);
			}
		}
		function handleKeyDownText(e: KeyboardEvent) {
			if (e.keyCode === 13) {
				if (ref && ref.current) {
					const element: HTMLElement = ref.current;
					element.blur();
				}
			}
		}

		let rowElement = (
			<span>
				{ text }
			</span>
		);
		if (selected) rowElement = (
			<input
				ref={ref}
				type="text"
				name={name}
				value={text}
				onBlur={handleBlurText}
				onKeyDown={handleKeyDownText}
			/>
		);
		if (!expandable) {
			return (
				<li
					ref={this.setRef}
					key={name}
					className={selected ? "selected" : undefined}
					style={itemStyle}
					onClick={ handleClickListItem }
				>
					{ rowElement }
				</li>
			);
		}
		return (
			<li
				ref={this.setRef}
				key={name}
				className={selected ? "selected" : undefined}
				style={itemStyle}
				onClick={ handleClickListItem }
			>
				<input
					type="checkbox"
					id={name}
					name={name}
					checked={toggled}
					onChange={ handleCheckChange }
					onClick={ handleCheckClick }
				/>
				<div/>
				{rowElement}
			</li>
		);
	}
};

export default ListItem;
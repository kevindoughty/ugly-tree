import { h, FunctionComponent } from "preact";
import { useRef } from "preact/hooks";
import { effect } from "@preact/signals";
import ListItem from "./ListItem.jsx";

import { Getters, Setters, Derived, Presentation, Id } from "./state.js";

type ListViewProps = {
	getters: Getters,
	setters: Setters,
	presentation: Presentation,
	derived: Derived
};
const ListView: FunctionComponent<ListViewProps> = ({getters, setters, presentation, derived}: ListViewProps) => {
	const ref = useRef(null);

	effect( () => {
		const value = presentation.listScroll.value;
		if (ref && ref.current) {
			const element: HTMLElement = ref.current;
			if (element.scrollTop !== value) element.scrollTop = value;
		}
	});

	const refReporter = function(element: HTMLElement) {
		element.scrollIntoView();
	}

	const onScroll = function(e: Event) {
		const element = e.target;
		if (element) {
			const scrollTop = (element as HTMLElement).scrollTop;
			setters.setScroll(scrollTop);
		}
	}

	const everything = derived.flattenedIdsLinear.value;
	const collapsed = derived.visibleCollapsedIds.value;
	const flattenedDepth = derived.flattenedDepthLookup.value;
	const visibleIds = derived.flattenedVisibleIds.value;
	const collapsedLength = collapsed.length;
	const everythingLength = everything.length;
	let collapsedIndex = 0;
	let visibleIndex = 0;
	let collapsedId = (collapsed.length) ? collapsed[collapsedIndex] : null;
	const children = [];

	for (let everythingIndex=0; everythingIndex<everythingLength; everythingIndex++) {
		const everythingId: Id = everything[everythingIndex];
		const id: Id = visibleIds[visibleIndex];
		if (everythingId === id) {
			visibleIndex++;
			const text: string = getters.getText(id);
			const childIds = getters.getChildIds(id);
			const expandable = childIds && childIds.length;
			const depth = flattenedDepth[everythingIndex];
			const selected = (id === presentation.selectedId.value);
			
			let toggled = false;
			if (collapsedId === id) {
				toggled = true;
				collapsedIndex++;
				collapsedId = (collapsedIndex<collapsedLength) ? collapsed[collapsedIndex] : null;
			}

			children.push(
				<ListItem
					id={id}
					text={text}
					expandable={expandable}
					toggled={toggled}
					selected={selected}
					depth={depth}
					refReporter={refReporter}
					setters={setters}
				/>
			);
		}
	}
	return (
		<ul 
			class="list"
			ref={ref}
			onScroll={ onScroll }	
		>
			{ children }
		</ul>
	);
}
export default ListView;

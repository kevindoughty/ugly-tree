import { 
	h,
	render,
	FunctionComponent,
	Fragment
} from "preact";

import Header from "./Header.jsx";
import ListView from "./ListView.jsx";

import { UndoManager, Localizer } from "@kvndy/undo-manager";
const undoLocalizer: Localizer = function(input) {
	return "Undo " + input;
}
const redoLocalizer: Localizer = function(input) {
	return "Redo " + input;
}
const manager = new UndoManager(undoLocalizer, redoLocalizer);

import { initState, State } from "./state.js";

const length = 20;
const state: State = initState(manager, length);

type AppProps = {
	state: State,
	manager: UndoManager
};
const App: FunctionComponent<AppProps> = ({state, manager}: AppProps) => {
	return (
		<>
			<Header
				setters={state.setters}
				presentation={state.presentation}
				manager={manager}
			/>
			<ListView
				getters={state.getters}
				setters={state.setters}
				presentation={state.presentation}
				derived={state.derived}
			/>
		</>
	);
}

render(<App
	state={state}
	manager={manager}
/>, document.body);

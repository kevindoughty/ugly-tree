const path = require("path");

module.exports = [{
	entry: "./temp/index.jsx",
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname) + "/public"
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/i,
				loader: "babel-loader",
			},
		],
	},
	resolve: {
		alias: { // Required for Preact
			"react": "preact/compat",
			"react-dom/test-utils": "preact/test-utils",
			"react-dom": "preact/compat", // Must be below test-utils
			"react/jsx-runtime": "preact/jsx-runtime",
		},
	},
}];
import { assert } from "chai";

function isFunction(w) {
	return w && {}.toString.call(w) === "[object Function]";
}

function exists(w) {
	return typeof w !== "undefined" && w !== null;
}

import { UndoManager } from "@kvndy/undo-manager";
import { initState } from "../temp/state.js";

describe("Test", function() {
	it("test", function() {
		assert(isFunction(function() {}));
		assert(isFunction(() => {}));
		assert(!isFunction({}));
		assert(!isFunction("[object Function]"));
		assert(isFunction(exists));
		assert(exists({}));
		assert(exists(false));
		assert(exists(0));
	});

	it("delete", function() {
		const manager = new UndoManager();
		const length = 0;
		const state = initState(manager, length);
		const { model, presentation, getters, setters, derived } = state;
		const { flattenedIdsLinear, flattenedDepthLookup, flattenedVisibleIds, visibleCollapsedIds, parentIds } = derived;
		const { setText, setScroll, disclosureToggle, addItem, deleteSelectedItem, selectListItem } = setters;
		// const { getText, getChildIds } = getters;
		// const { allCollapsedIds, selectedId, listScroll } = presentation;
		// const { tree, nextId } = model;

		assert.deepEqual(flattenedIdsLinear.value, []);
		assert.deepEqual(flattenedDepthLookup.value, []);
		assert.deepEqual(flattenedVisibleIds.value, []);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { });

		addItem();
		assert.deepEqual(flattenedIdsLinear.value, [1]);
		assert.deepEqual(flattenedDepthLookup.value, [1]);
		assert.deepEqual(flattenedVisibleIds.value, [1]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "1":0 });

		addItem();
		assert.deepEqual(flattenedIdsLinear.value, [1,2]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2]);
		assert.deepEqual(flattenedVisibleIds.value, [1,2]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "1":0, "2":1 });

		selectListItem(0);
		addItem();
		assert.deepEqual(flattenedIdsLinear.value, [1,2,3]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2,1]);
		assert.deepEqual(flattenedVisibleIds.value, [1,2,3]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "1":0, "2":1, "3":0 });

		addItem();
		assert.deepEqual(flattenedIdsLinear.value, [1,2,3,4]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2,1,2]);
		assert.deepEqual(flattenedVisibleIds.value, [1,2,3,4]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "1":0, "2":1, "3":0, "4":3 });
		
		disclosureToggle(1);
		disclosureToggle(3);
		assert.deepEqual(flattenedIdsLinear.value, [1,2,3,4]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2,1,2]);
		assert.deepEqual(flattenedVisibleIds.value, [1,3]);
		assert.deepEqual(visibleCollapsedIds.value, [1,3]);
		assert.deepEqual(parentIds.value, { "1":0, "2":1, "3":0, "4":3 });
		
		selectListItem(1);
		deleteSelectedItem(false);
		assert.deepEqual(flattenedIdsLinear.value, [3,4]);
		assert.deepEqual(flattenedDepthLookup.value, [1,2]);
		assert.deepEqual(flattenedVisibleIds.value, [3]);
		assert.deepEqual(visibleCollapsedIds.value, [3]);
		assert.deepEqual(parentIds.value, { "3":0, "4":3 });

		selectListItem(3);
		deleteSelectedItem(true);

		assert.deepEqual(flattenedIdsLinear.value, [4]);
		assert.deepEqual(flattenedDepthLookup.value, [1]);
		assert.deepEqual(flattenedVisibleIds.value, [4]);
		assert.deepEqual(visibleCollapsedIds.value, []);
		assert.deepEqual(parentIds.value, { "4":0 });

	});

});
# ugly-tree

A tree view example that preserves selection and collapsed/expanded state through undo and redo. Actually a list. Ugly because it depends on state shape.

[https://kevindoughty.gitlab.io/ugly-tree/index.html](https://kevindoughty.gitlab.io/ugly-tree/index.html)